package com.metric.orca.security.utility.security;

import java.security.MessageDigest ;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class TDES {
	
	private static String TDESKey="";
	
	public byte[] encrypt(String message) throws Exception {
	      final MessageDigest md = MessageDigest.getInstance("md5");
	      final byte[] digestOfPassword = md.digest(getTDESKey().getBytes("utf-8"));
	      final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
	      
	      for (int j = 0, k = 16; j < 8;) {
	              keyBytes[k++] = keyBytes[j++];
	      }

	      final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
	      final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
	      final Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
	      cipher.init(Cipher.ENCRYPT_MODE, key, iv);

	      final byte[] plainTextBytes = message.getBytes("utf-8");
	      final byte[] cipherText = cipher.doFinal(plainTextBytes);
	     
	      
	      return cipherText;
	  }

	  public String decrypt(String encodedWord) throws Exception {
		  
		  final byte[] message = new sun.misc.BASE64Decoder().decodeBuffer(encodedWord); 
	      final MessageDigest md = MessageDigest.getInstance("md5");
	      final byte[] digestOfPassword = md.digest(getTDESKey().getBytes("utf-8"));
	      final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
	      for (int j = 0, k = 16; j < 8;) {
	              keyBytes[k++] = keyBytes[j++];
	      }

	      final SecretKey key = new SecretKeySpec(keyBytes, "DESede");
	      final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
	      final Cipher decipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
	      decipher.init(Cipher.DECRYPT_MODE, key, iv);

	      final byte[] plainText = decipher.doFinal(message);
	      return new String(plainText, "UTF-8");
	  }	  	 
	  
	  @SuppressWarnings("static-access")
	public void setTDESKey(String TDESKey)
	  {
		  this.TDESKey=TDESKey;		  
	  }
	  public String getTDESKey()
	  {
		  return TDESKey;		  
	  }
}

