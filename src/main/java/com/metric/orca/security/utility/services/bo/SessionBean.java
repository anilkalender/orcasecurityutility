package com.metric.orca.security.utility.services.bo;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import com.core.service.JCLCoreServiceException;
import com.core.service.bo.login.JINLoginBO;
import com.core.service.vo.login.JINLoginVO;
import com.metric.orca.security.utility.model.LoginBaseModel;

@Service
public class SessionBean {

	@Autowired
	private ApplicationContext appContext;

	private JINLoginBO loginBO;

	public JINLoginBO getLoginBO() {
		if (loginBO == null) {
			loginBO = (JINLoginBO) appContext.getBean(JINLoginBO.XI_SERVICE_NAME);
		}
		return loginBO;
	}

	private static Map<String, Map<String, JINLoginVO>> enterpriseUserSessionMap;

	public SessionBean() {
		if (enterpriseUserSessionMap == null) {
			enterpriseUserSessionMap = new HashMap<String, Map<String, JINLoginVO>>();
		}
	}

	private Map<String, JINLoginVO> getEnterpriseUserSessionMap(String username) {
		return enterpriseUserSessionMap.get(username);
	}

	public JINLoginVO getEnterpriseUserSessionInfo(LoginBaseModel loginModel) throws JCLCoreServiceException {

		Map<String, JINLoginVO> lOBMap = getEnterpriseUserSessionMap(loginModel.getSapBoUsername());

		if (lOBMap == null) {
			return null;
		}

		JINLoginVO lOBLoginVO = lOBMap.get(loginModel.getSapBoUrl());

		if (lOBLoginVO == null) {
			return null;
		}

		if (!getLoginBO().isSessionActive(lOBLoginVO.getSession())) {
			removeEnterpriseUserSessionInfo(loginModel);
			return null;
		}

		return lOBLoginVO;
	}

	public Map<String, JINLoginVO> removeEnterpriseUserSessionInfo(String username) {
		Map<String, JINLoginVO> lOBUserSystemMap = getEnterpriseUserSessionMap(username);
		if (lOBUserSystemMap != null) {
			// remove from map
			enterpriseUserSessionMap.remove(username);
		}
		return lOBUserSystemMap;
	}

	public void addEnterpriseUserSessionInfo(LoginBaseModel loginModel, JINLoginVO loginObject) {

		Map<String, JINLoginVO> lOBUserSystemMap = getEnterpriseUserSessionMap(loginModel.getSapBoUsername());
		if (lOBUserSystemMap == null) {
			lOBUserSystemMap = new HashMap<String, JINLoginVO>();
			enterpriseUserSessionMap.put(loginModel.getSapBoUsername(), lOBUserSystemMap);
		}
		lOBUserSystemMap.put(loginModel.getSapBoUrl(), loginObject);

	}

	public Map<String, JINLoginVO> removeEnterpriseUserSessionInfo(LoginBaseModel loginModel) {
		Map<String, JINLoginVO> lOBUserSystemMap = getEnterpriseUserSessionMap(loginModel.getSapBoUsername());
		if (lOBUserSystemMap != null) {
			// remove from map
			lOBUserSystemMap.remove(loginModel.getSapBoUrl());
		}
		return lOBUserSystemMap;
	}
}
