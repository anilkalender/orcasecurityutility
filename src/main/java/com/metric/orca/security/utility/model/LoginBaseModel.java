package com.metric.orca.security.utility.model;

public class LoginBaseModel {

	private String sapBoUrl;
	private String sapBoUsername;
	private String sapBoPassword;
	
	public LoginBaseModel(String sapBoUrl, String sapBoUsername, String sapBoPassword) {
		super();
		this.sapBoUrl = sapBoUrl;
		this.sapBoUsername = sapBoUsername;
		this.sapBoPassword = sapBoPassword;
	}
	
	public LoginBaseModel(){}
	
	public String getSapBoUrl() {
		return sapBoUrl;
	}
	public void setSapBoUrl(String sapBoUrl) {
		this.sapBoUrl = sapBoUrl;
	}
	public String getSapBoUsername() {
		return sapBoUsername;
	}
	public void setSapBoUsername(String sapBoUsername) {
		this.sapBoUsername = sapBoUsername;
	}
	public String getSapBoPassword() {
		return sapBoPassword;
	}
	public void setSapBoPassword(String sapBoPassword) {
		this.sapBoPassword = sapBoPassword;
	}
}
