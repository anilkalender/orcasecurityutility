package com.metric.orca.security.utility.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import com.google.gson.Gson;
import com.metric.orca.security.utility.model.LoginBaseModel;
import com.metric.orca.security.utility.services.bo.BOUtilService;
import com.metric.orca.security.utility.util.RequestWrapper;
import com.metric.orca.security.utility.util.ResponseWrapper;

@Component
@Scope(value = "prototype")
public class BoLoginFilter extends OncePerRequestFilter implements ApplicationContextAware {

	private static ApplicationContext ctx = null;

	@Autowired
	BOUtilService boLoginService;

	private static final Logger LOGGER = LoggerFactory.getLogger("com.metric.requestLog");

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws ServletException, IOException {

		if (req.getMethod().equals("POST")) {

			RequestWrapper newReq = null;

			Gson gson = new Gson();

			try {
				newReq = new RequestWrapper(req);

				String body = newReq.getBody();

				LoginBaseModel loginModel = gson.fromJson(body, LoginBaseModel.class);
				// decode for password
				String loginObjStr = gson.toJson(loginModel.getSapBoPassword());
				LOGGER.debug("request : " + req.getRequestURL());
				LOGGER.debug(body.replace(loginObjStr, "***"));
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
			}

			req = newReq;
		}
		try {
			ResponseWrapper wrappedResponse = new ResponseWrapper((HttpServletResponse) res);
			chain.doFilter(req, wrappedResponse);
			byte[] bytes = wrappedResponse.getByteArray();
			res.getOutputStream().write(bytes);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		ctx = applicationContext;
	}

	public ApplicationContext getApplicationContext() {
		return ctx;
	}
}