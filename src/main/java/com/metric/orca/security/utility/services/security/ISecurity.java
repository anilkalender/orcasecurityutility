package com.metric.orca.security.utility.services.security;

import java.util.List;

import com.metric.orca.security.utility.model.LoginBaseModel;
import com.metric.orca.security.utility.model.UserModel;

public interface ISecurity {

	boolean syncUsers(LoginBaseModel loginBaseModel) throws Exception;

	List<UserModel> getSyncUsers(LoginBaseModel loginBaseModel) throws Exception;

}
