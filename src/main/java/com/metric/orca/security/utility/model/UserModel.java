package com.metric.orca.security.utility.model;

public class UserModel {
	
	private String title;
	private String fullName;
	private int id;
	private String cuid;
	private String mailAdress;
	
	public UserModel(String title, String fullName, int id, String cuid, String mailAdress) {
		super();
		this.title = title;
		this.fullName = fullName;
		this.id = id;
		this.cuid = cuid;
		this.mailAdress = mailAdress;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCuid() {
		return cuid;
	}
	public void setCuid(String cuid) {
		this.cuid = cuid;
	}
	public String getMailAdress() {
		return mailAdress;
	}
	public void setMailAdress(String mailAdress) {
		this.mailAdress = mailAdress;
	}
	
	
}
