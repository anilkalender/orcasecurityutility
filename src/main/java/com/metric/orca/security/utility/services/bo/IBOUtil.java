package com.metric.orca.security.utility.services.bo;

import com.core.service.vo.login.JINLoginVO;
import com.metric.orca.security.utility.model.LoginBaseModel;
import com.pro.core.exception.JCLServiceException;

public interface IBOUtil {

	public JINLoginVO loginBO(LoginBaseModel loginModel) throws JCLServiceException, Exception;
	
}