package com.metric.orca.security.utility.model.output;

public class OutputModel {

	private String result;
	private String errorText;
	private Object data;

	public OutputModel() {

	}

	public OutputModel(String result, String errorText, Object data) {
		super();
		this.result = result;
		this.errorText = errorText;
		this.data = data;
	}
	
	public OutputModel(String result, String errorText) {
		super();
		this.result = result;
		this.errorText = errorText;
	}

	@Override
	public String toString() {
		return "OutputModel [result=" + result + ", errorText=" + errorText + "]";
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getErrorText() {
		return errorText;
	}

	public void setErrorText(String errorText) {
		this.errorText = errorText;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public static OutputModel createSuccessResponseWithData(OutputModel model) {

		model.setResult("1");
		return model;
	}

	public static OutputModel createSuccessResponseWithData(Object data) {

		return new OutputModel("1", "", data);
	}

	public static OutputModel createErrorResponseWithData(String errorMessage) {
		return new OutputModel("0", errorMessage);
	}
}