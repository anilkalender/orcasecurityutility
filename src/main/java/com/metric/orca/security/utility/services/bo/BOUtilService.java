package com.metric.orca.security.utility.services.bo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import com.core.service.bo.login.JINLoginBO;
import com.core.service.vo.login.JINLoginVO;
import com.metric.orca.security.utility.model.LoginBaseModel;
import com.metric.orca.security.utility.security.TDESGenerator;
import com.metric.orca.security.utility.services.BaseService;

@Service
public class BOUtilService extends BaseService implements IBOUtil {

	@Autowired
	private ApplicationContext appContext;

	private static final Logger LOGGER = LoggerFactory.getLogger(BOUtilService.class);

	@Autowired
	@Qualifier(JINLoginBO.XI_SERVICE_NAME)
	JINLoginBO loginBO;

	@Autowired
	SessionBean sessionBean;

	@Override
	public JINLoginVO loginBO(LoginBaseModel loginModel) throws Exception {

		checkLoginInputParameters(loginModel);

		JINLoginVO loginObject = sessionBean.getEnterpriseUserSessionInfo(loginModel);

		if (loginObject != null) {
			sessionBean.removeEnterpriseUserSessionInfo(loginModel);
		}

		loginObject = (JINLoginVO) appContext.getBean(JINLoginVO.VO_NAME_XI);

		loginObject.setAuthenticationType(JINLoginVO.AUTHENTICATE_TYPE_BO_ENTERPRISE);
		loginObject.setPassword(TDESGenerator.deEncrpt(loginModel.getSapBoPassword()));
		loginObject.setServerName(loginModel.getSapBoUrl());
		loginObject.setUsername(loginModel.getSapBoUsername());

		loginObject = loginBO.login(loginObject);
		
		try {
			sessionBean.addEnterpriseUserSessionInfo(loginModel, loginObject);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}

		return loginObject;
	}

	private void checkLoginInputParameters(LoginBaseModel loginModel) throws Exception {

		if (loginModel.getSapBoPassword() == null || loginModel.getSapBoPassword().isEmpty()) {
			throw new Exception("sapBoPassword field is required");
		}

		if (loginModel.getSapBoUrl() == null || loginModel.getSapBoUrl().isEmpty()) {
			throw new Exception("sapBoUrl field is required");
		}

		if (loginModel.getSapBoUsername() == null || loginModel.getSapBoUsername().isEmpty()) {
			throw new Exception("sapBoUsername is required");
		}
	}

	public void logoffBO(JINLoginVO loginXIVO) {
		try {
			loginBO.logoff(loginXIVO);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
}
