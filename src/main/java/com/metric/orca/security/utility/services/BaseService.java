package com.metric.orca.security.utility.services;

import java.util.Date;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import com.bor4.service.dao.metadata.report.JINReportMetadataDAO;
import com.core.application.vo.RepositoryModel;
import com.core.application.vo.RepositoryUserModel;
import com.core.application.vo.UserModel;
import com.core.service.bo.common.JINCommonServiceBO;
import com.core.service.bo.connection.JINConnectionBO;
import com.core.service.bo.customrole.JINCustomRoleBO;
import com.core.service.bo.folder.JINFolderBO;
import com.core.service.bo.login.JINLoginBO;
import com.core.service.bo.session.Context;
import com.core.service.bo.universe.JINUniverseBO;
import com.core.service.bo.user.JINUserBO;
import com.core.service.bo.usergroup.JINUserGroupBO;
import com.core.service.dao.document.JINReportDAO;
import com.core.service.vo.login.JINLoginVO;
import com.pro.core.exception.JCLServiceException;

public class BaseService {
	@Autowired
	private ApplicationContext appContext;

	private Context getContext(JINLoginVO loginVO) {

		RepositoryModel repositoryVO = getRepositoryModel(loginVO);
		Context context = new Context(repositoryVO);
		context.setSystemId(1L);
		context.setSessionUser(loginVO.getUsername());
		context.setToken(loginVO.getLogonToken());
		context.setBoSession(loginVO);

		return context;
	}

	private RepositoryModel getRepositoryModel(JINLoginVO loginVO) {
		RepositoryModel repositoryVO = new RepositoryModel();
		repositoryVO.setActive(true);
		repositoryVO.setBoPort(6400);
		repositoryVO.setDesc(loginVO.getServerName());
		repositoryVO.setId(1L);
		repositoryVO.setLoginType(0);
		repositoryVO.setName(loginVO.getServerName());
		repositoryVO.setRestfulPort(6405);
		repositoryVO.setScreenName(loginVO.getServerName());
		repositoryVO.setType("BusinessObjects");
		repositoryVO.setUrl(loginVO.getServerName());
		RepositoryUserModel repositoryUser = new RepositoryUserModel();
		repositoryUser.setActive(true);
		repositoryUser.setAuthenticationType(0);
		repositoryUser.setConnection(true);
		repositoryUser.setId(0);
		repositoryUser.setName(loginVO.getUsername());
		repositoryUser.setPassword(loginVO.getPassword());
		UserModel createdby = new UserModel();
		createdby.setActive(1);
		createdby.setEmail("no mail");
		createdby.setId(1L);
		createdby.setLastLoginTime(new Date());
		createdby.setPassword(loginVO.getPassword());
		createdby.setReset(0);
		createdby.setRoles(null);
		createdby.setType("no type");
		createdby.setUsername(loginVO.getUsername());
		repositoryUser.setCreatedby(createdby);
		repositoryVO.setRepositoryUser(repositoryUser);
		return repositoryVO;
	}

	public JINUserBO getUserService(JINLoginVO loginVO) throws JCLServiceException {
		return (JINUserBO) appContext.getBean(JINUserBO.XI_SERVICE_NAME, getContext(loginVO));
	}

	public JINUserGroupBO getGroupService(JINLoginVO loginVO) throws JCLServiceException {
		return (JINUserGroupBO) appContext.getBean(JINUserGroupBO.XI_SERVICE_NAME, getContext(loginVO));
	}

	public JINCommonServiceBO getBusinessObjectService(JINLoginVO loginVO) throws JCLServiceException {
		return (JINCommonServiceBO) appContext.getBean(JINCommonServiceBO.XI_SERVICE_NAME, getContext(loginVO));
	}

	public JINUniverseBO getUniverseService(JINLoginVO loginVO) throws JCLServiceException {
		return (JINUniverseBO) appContext.getBean(JINUniverseBO.XI_SERVICE_NAME, getContext(loginVO));
	}

	public JINFolderBO getFolderService(JINLoginVO loginVO) throws JCLServiceException {
		return (JINFolderBO) appContext.getBean(JINFolderBO.XI_SERVICE_NAME, getContext(loginVO));
	}

	public JINCustomRoleBO getCustomRoleService(JINLoginVO loginVO) {
		return (JINCustomRoleBO) appContext.getBean(JINCustomRoleBO.XI_SERVICE_NAME, getContext(loginVO));
	}

	public JINConnectionBO getConnectionService(JINLoginVO loginVO) {
		return (JINConnectionBO) appContext.getBean(JINConnectionBO.XI_SERVICE_NAME, getContext(loginVO));
	}

	public JINReportMetadataDAO getReportMetadataService(JINLoginVO loginVO) {
		return (JINReportMetadataDAO) appContext.getBean(JINReportMetadataDAO.SERVICE_NAME_4X, getRepositoryModel(loginVO),loginVO.getLogonToken());
	}
	
	public JINLoginBO getRestLoginBOService() {
		return (JINLoginBO) appContext.getBean(JINLoginBO.SERVICE_NAME_RESTFUL);
	}
	
	public JINReportDAO getReportService(JINLoginVO loginVO) {
		return (JINReportDAO) appContext.getBean(JINReportDAO.SERVICE_NAME, getRepositoryModel(loginVO));
	}
	
}
