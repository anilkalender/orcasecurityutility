
package com.metric.orca.security.utility.controller.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.metric.orca.security.utility.model.LoginBaseModel;
import com.metric.orca.security.utility.model.output.OutputModel;
import com.metric.orca.security.utility.security.TDESGenerator;
import com.metric.orca.security.utility.services.security.ISecurity;
import com.pro.core.exception.JCLServiceException;

@RestController
@RequestMapping(value = "/restful/security/sync", produces = "application/json")
public class SecurityController {

	@Autowired
	private ISecurity iReport;

	private static final Logger logger = LoggerFactory.getLogger(SecurityController.class);

	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public OutputModel resetSecurity(@RequestBody LoginBaseModel loginBaseModel) throws Exception {
		try {
			return OutputModel.createSuccessResponseWithData(iReport.syncUsers(loginBaseModel));
		} catch (JCLServiceException e) {
			logger.error(e.getMessage(), e);
			return OutputModel.createErrorResponseWithData(e.getMessage());
		}
	}

	
	@RequestMapping(value = "/getUsers", method = RequestMethod.POST)
	public OutputModel getResetSecurity(@RequestBody LoginBaseModel loginBaseModel) throws Exception {
		try {
			return OutputModel.createSuccessResponseWithData(iReport.getSyncUsers(loginBaseModel));
		} catch (JCLServiceException e) {
			logger.error(e.getMessage(), e);
			return OutputModel.createErrorResponseWithData(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/encodePassword", method = RequestMethod.POST)
	public OutputModel encode(@RequestBody LoginBaseModel loginBaseModel) throws Exception {
		try {
			loginBaseModel.setSapBoPassword(TDESGenerator.Encrpt(loginBaseModel.getSapBoPassword()));
			return OutputModel.createSuccessResponseWithData(loginBaseModel);
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
			return OutputModel.createErrorResponseWithData(e.getMessage());
		}
	}

}
