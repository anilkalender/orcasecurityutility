package com.metric.orca.security.utility.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;

import com.metric.orca.security.utility.filter.BoLoginFilter;

@EnableAsync
@SpringBootApplication(scanBasePackages = "com")
public class OrcaSecurityUtilityApplication {

	@Autowired
	BoLoginFilter boLoginFilter;

	public static void main(String[] args) {
		SpringApplication.run(OrcaSecurityUtilityApplication.class, args);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Bean
	public FilterRegistrationBean someFilterRegistration() {

		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(boLoginFilter);
		registration.addUrlPatterns("/*");
		registration.setName("boLoginFilter");
		registration.setOrder(1);
		return registration;
	}
}