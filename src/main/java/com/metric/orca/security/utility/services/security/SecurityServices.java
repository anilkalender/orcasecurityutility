package com.metric.orca.security.utility.services.security;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.core.service.bo.common.JINCommonServiceBO;
import com.core.service.vo.JINBusinessObjectsVO;
import com.core.service.vo.login.JINLoginVO;
import com.metric.orca.security.utility.model.LoginBaseModel;
import com.metric.orca.security.utility.model.UserModel;
import com.metric.orca.security.utility.services.BaseService;
import com.metric.orca.security.utility.services.bo.IBOUtil;
import com.pro.core.utilities.JENDataKind;

@Service
public class SecurityServices extends BaseService implements ISecurity {

	@Autowired
	private IBOUtil iBOUtil;

	@Value("${DomainUsersGroupId}")
	private int domainUsersGroupId;

	int DeletedUserCount = 0;

	private static final Logger LOGGER = LoggerFactory.getLogger(SecurityServices.class);

	@Override
	public boolean syncUsers(LoginBaseModel loginBaseModel) throws Exception {

		JINLoginVO loginVO = iBOUtil.loginBO(loginBaseModel);

		JINCommonServiceBO boService = getBusinessObjectService(loginVO);
		List<JINBusinessObjectsVO> AllUserOnBO = boService.getAllObjectsByKind(JENDataKind.USER);

		AllUserOnBO.stream().filter(user -> !user.isDisabled()).filter(user -> user.getGroups().size() == 2)
				.forEach(user -> {

					for (Integer groupId : user.getGroups()) {
						if (groupId == domainUsersGroupId) {
							try {
								 boService.deleteUserNow(user.getTitle());
								LOGGER.debug("Deleted user (Name|ID|CUID) :  " + user.getTitle() + " | " + user.getID()
										+ " | " + user.getCUID());
								LOGGER.debug("Group owned by the user for the delete : " + groupId);
								DeletedUserCount++;
							} catch (Exception e) {
								LOGGER.error(e.getMessage(), e);
							}
						}
					}
				});

		LOGGER.debug("Only everyone members size : " + DeletedUserCount);
		DeletedUserCount = 0;

		return true;
	}

	@Override
	public List<UserModel> getSyncUsers(LoginBaseModel loginBaseModel) throws Exception {

		LOGGER.info("getSyncUsers Started..");

		JINLoginVO loginVO = iBOUtil.loginBO(loginBaseModel);

		JINCommonServiceBO boService = getBusinessObjectService(loginVO);
		List<JINBusinessObjectsVO> AllUserOnBO = boService.getAllObjectsByKind(JENDataKind.USER);

		List<UserModel> deletedUsers = new ArrayList<UserModel>();

		AllUserOnBO.stream().filter(user -> !user.isDisabled()).filter(user -> user.getGroups().size() == 2)
				.forEach(user -> {

					for (Integer groupId : user.getGroups()) {
						if (groupId == domainUsersGroupId) {
							try {
								deletedUsers.add(new UserModel(user.getTitle(), user.getFullName(), user.getID(),
										user.getCUID(), user.getEmailAddress()));
							} catch (Exception e) {
								LOGGER.error(e.getMessage(), e);
							}
						}
					}
				});

		LOGGER.info("getSyncUsers Ended.");

		return deletedUsers;
	}
}