package com.metric.orca.security.utility.trigger;

import javax.annotation.PostConstruct;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;
import org.springframework.stereotype.Service;

import com.metric.orca.security.utility.job.SecurityJob;
import com.metric.orca.security.utility.quartz.config.AutoWiringSpringBeanJobFactory;

@Service
public class SecurityJobTrigger {

	@Autowired
	private ApplicationContext applicationContext;

	@Value("${QuartzExpression}")
	private String quartzExpression;

	@PostConstruct
	public void init() {

		try {
			triggerSecuritySyncList();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void triggerSecuritySyncList() throws Exception {

		JobDetail job = JobBuilder.newJob(SecurityJob.class).withIdentity("SecurityJob", "group1").build();

		Trigger trigger = TriggerBuilder.newTrigger().withIdentity("triggerSecuritySyncList", "group1")
				.withSchedule(CronScheduleBuilder.cronSchedule(quartzExpression)).build();

		// schedule it
		Scheduler scheduler = new StdSchedulerFactory().getScheduler();
		scheduler.setJobFactory(springBeanJobFactory());
		scheduler.start();
		scheduler.scheduleJob(job, trigger);
	}

	private SpringBeanJobFactory springBeanJobFactory() {
		AutoWiringSpringBeanJobFactory jobFactory = new AutoWiringSpringBeanJobFactory();
		jobFactory.setApplicationContext(applicationContext);
		return jobFactory;
	}
}
