package com.metric.orca.security.utility.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.metric.orca.security.utility.model.LoginBaseModel;
import com.metric.orca.security.utility.services.security.SecurityServices;

public class SecurityJob implements Job {

	private static final Logger logger = LoggerFactory.getLogger(SecurityJob.class);

	@Autowired
	SecurityServices securityServices;

	@Value("${boCMCName}")
	private String boCMCName;

	@Value("${boUserName}")
	private String boUserName;

	@Value("${boUserPass}")
	private String boUserPass;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		try {
			logger.info(" Job Started..");
			LoginBaseModel loginModel = new LoginBaseModel(boCMCName, boUserName, boUserPass);
			securityServices.syncUsers(loginModel);
			logger.info(" Job Ended.");
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
	}
}